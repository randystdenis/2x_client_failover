#!/usr/bin/python
import urllib, urllib2, json, socket, requests, logging, os, netsnmp, subprocess, fileinput, time, threading, sys
from logging.handlers import SysLogHandler

socket.setdefaulttimeout(1)

level = logging.DEBUG
logger = logging.getLogger('FailoverLogger')
handler = SysLogHandler(address='/dev/log')
logger.addHandler(handler)
logger.setLevel(level)


# def openURL(url):
#   """ 
#   Open a URL and return the JSON

#   Parameters:
#     url: the url to fetch the JSON object from
#   """
#   logger.debug('Fetching data from url: %s', url)
#   try:
#     r = urllib2.urlopen(url, timeout=1);
#   except Exception as e:
#     logger.error('Error: %s', e)
  
#   try:
#     rd = json.loads(r.read())
#   except Exception as e:
#     logger.error('Error: %s', e)
#     rd = ""

#   logger.debug('json returned from %s is %s', url, rd)
#   return rd



def readFile(name, d=None):
  """
  Read info from a file, if the second parameter is set then it is used to
  Write data out to a file, otherwise the contents are read and returned

  Parameters:
    name: the name of the file to process (should be in the same direcotry)
    d: the contents to write to the file
  """
  logger.debug('Reading file: %s', name)
  if d is None:
    if os.path.isfile(name):
      try:
        f = open(name, 'r')
        logger.debug('Opened file: %s', name)
      except IOError as e:
        logger.error('Unable to open file %s', name)
    else:
      logger.error('File missing! creating file: %s', name)
      try:
        open(name, 'a').close()
        f = open(name, 'r')
      except Exception as e:
        logger.error('Error: %s', e)

    contents = f.read().strip()

    try:
      f.close()
    except Exception as e:
      logger.error('Error: %s', e)

    return contents

  else:
    try:
      f = open(name, 'w')
      logger.debug('Opened file: %s for writing.', name)
      f.write(d)
      logger.debug('Wrote \"%s\" to file: %s', d, name)
    except Exception as e:
      logger.error('Error: %s', e)
      return False

    try:
      f.close()
      logger.debug('Closed file: %s', name)
    except IOError as e:
      logger.error('Error: %s', e.reason)

    return True




def processChanges(lmDate, storedDate):
  """
  If there were changes made then we will need to process data (ie, fetch files, update files, etc
  This is used to read the last modifed date and compare it to the stored date

 Parameters:
     lmDate: Last modified date of the file on the server
     storeDate: The date from the storeFile that contains the last time the file was modified
  """
  logger.debug('Processing potential changes from \"%s\" and \"%s\"', lmDate, storedDate)
  if lmDate == storedDate:
    logger.debug('No differences found in date, nothing to do')
    return True
  else:
    logger.debug('Dates do not match, change in content detected!')
    return False




def downloadFile(path, fileName, url, force):
  """
  Download a file from a provided URL

  Parameters:
    path: the path to save the file to
    fileName: the file name to save the file as in the specified path
    url: the url (filename included) to download the file from
    force: True or False to force over writing of an existing file
  """
  if force == True:
    try:
      logger.debug('Downloading file from url: %s and saving it as filename: %s', url, fileName)
      r = urllib2.urlopen(url).read()
      with open(os.path.join(path, fileName), "w") as f:
        f.write(r)
    except Exception as e:
      logger.error('Error: %s', e)

  else:
    if not os.path.exists(path + fileName):
      try:
        logger.debug('Downloading file from url: %s and saving it as filename: %s', url, fileName)
        r = urllib2.urlopen(url).read()
        with open(os.path.join(path, fileName), "w") as f:
          f.write(r)
      except Exception as e:
        logger.error('Error: %s', e)
    else:
      logger.debug("File already exsits, skipping")




def downloadConfigFiles(fIP):
  """
  Download all the config files from the neghbour device

  Parameters:
    fIP: the IP address to connect to to fetch files
  """
  filesToDownload = ['config_VGA-0.json', 'media.json', 'playlist.json', 'region.json', 'resources.json', 'schedule.json', 'time_to_play.json']

  directory = "/opt/content2"
  try:
    if not os.path.exists(directory):
      os.makedirs(directory)
  except Exception as e:
    logger.error('Error: %s', e)


  for f in filesToDownload:
    url = "http://" + fIP + "/" + f
    downloadFile(directory, f, url, True)




def downloadMedia(fIP):
  """
  Parse the playlist and media json files from the neighbour machine then download
  all the media files and save them in the /opt/media/ directory

  Parameters:
    fIP: The IP address to connect to to download media from
  """
  playlist = ""
  media = ""

  try:
    playlist = json.loads(open("/opt/content2/playlist.json").read())
    media = json.loads(open("/opt/content2/media.json").read())
  except Exception as e:
    logger.error('Error: %s', e)

  directory = "/opt/media2"
  try:
    if not os.path.exists(directory):
      os.makedirs(directory)
  except Exception as e:
    logger.error('Error: %s', e)

  if playlist != "" and media != "":
    for e in playlist:
      for media_id in e["assignment_ids"]:
        ma_id = media[media_id -1]["media_asset_id"]

        url = "http://" + fIP + "/media/" + str(ma_id)
        downloadFile("/opt/media2/", str(ma_id), url, False)
  else:
    logger.error('Unable to download file because playlist and media json files are non-existant or empty')




def determineMaster():
  """
  Determines what media engine is the master
  """
  try:
    config = json.loads(open("/opt/content/config.json").read())
    orig = config["message_url"]
    master = orig.replace(":15526", "")
  except Exception as e:
    logger.error("config.json empty, master must be localhost")
    master = "http://localhost"
  return master




def getUptime(fIP):
  """
  Get the uptime of the neighbour machine by executing an snmp query to retrieve the hrSystemUptime
  If a timeout or anythine occurs while this is fetched then 0 is returned

  Parameters:
    fIP: The IP address of the media engine being monitored
  """
  res = netsnmp.snmpget(netsnmp.Varbind('HOST-RESOURCES-MIB::hrSystemUptime', 0), Version = 2, DestHost=fIP, Community='me3', Timeout=1000000, Retries=0)

  if res[0]:
    return int(res[0])
  else:
    return 0




def compareUptime(fIP, uptime):
  """
  Compare the uptime previously retrieved to the currently fetched uptime
  Uptime set to 0 on startup of the script or after reboot so any uptime
  retrieved will be higher than the initial uptime and failover will not happen

  Parameters:
    fIP: the IP address of the neighbour media engion
    uptime: The uptime from the last update
  """
  newUptime = getUptime(fIP)

  if newUptime == 0 and uptime != 0:
    logger.error('Uptime returned was 0 which most likely means the system is offline, intiating takeover')
    return False
  elif newUptime < uptime:
    logger.error('Uptime was %s and previous uptime was %s.  Failure in takover device detected, initiating takeover', newUptime, uptime)
    return False
  else:
    # Keep on keepin' on
    logger.debug('Uptime was %s which is greater than or equal to %s, everthing functioning as expected', newUptime, uptime)
    return newUptime




def executeCommand(retry, numOfRetries, command, args=None):
  """
  Execute a command local to the machine

  Parameters:
    retry: True or False on whether or not to retry
    numOfRetries: The number of times to try to execute the command
    command: the command to execute (eg. ls)
    args OPTIONAL: any arguments to pass to the command (eg. -al for the previous ls command)
  """
  logger.debug('Executing command: %s', command)
  retryCount = 0
  FNULL = open(os.devnull, 'w')
  if args is None:
    result = subprocess.call(command, shell=True, stdout=FNULL, stderr=subprocess.STDOUT)
    if result is 0:
      logger.debug('Command: %s with arguments %s ran succesfully', command, args)
    else:
      if retry is True:
        while retryCount != numOfRetries:
          retryCount += 1
          time.sleep(2)
          result = subprocess.call(command, shell=True, stdout=FNULL, stderr=subprocess.STDOUT)
          if result is 0:
            logger.debug('Command: %s with arguments %s ran succesfully', command, args)
            retryCount = numOfRetries
      else:
        logger.error('Command: %s with arguments: %s failed to run, return code: %s', command, args, result)
  else:
    result = subprocess.call([command, args], shell=True, stdout=FNULL, stderr=subprocess.STDOUT)
    if result is 0:
      logger.debug('Command: %s with arguments %s ran succesfully', command, args)
    else:
      if retry is True:
        while retryCount != numOfRetries:
          retryCount += 1
          time.sleep(2)
          result = subprocess.call([command, args], shell=True, stdout=FNULL, stderr=subprocess.STDOUT)
          if result is 0:
            logger.debug('Command: %s with arguments %s ran succesfully', command, args)
            retryCount = numOfRetries
      else:
        logger.error('Command: %s with arguments: %s failed to run, return code: %s', command, args, result)




def setScreen(a, b, c=None, d=None, e=None, f=None, g=None):
  """
  Set the input of the screen attached to the serial input by using the command

  lgutil -t 2000 "a 01 b c d e f g"

  The only required parameters are a and b, the rest are optional

  Parameters:
    a: first argument
    b: second argument
    c OPTIONAL: third argument
    d OPTIONAL: forth argument
    e OPTIONAL: fifth argument
    f OPTIONAL: sixth argument
    g OPTIONAL: seventh argument
  """
  result = 0
  args = ""
  cmd = "lgutil"

  if c is None:
    args = "-t 2000 \"" + a + " 01 " + b + "\""
  elif d is None:
    args = "-t 2000 \"" + a + " 01 " + b + " " + c + "\""
  elif e is None:
    args = "-t 2000 \"" + a + " 01 " + b + " " + c + " " + d + "\""
  elif f is None:
    args = "-t 2000 \"" + a + " 01 " + b + " " + c + " " + d + " " + e + "\""
  elif g is None:
    args = "-t 2000 \"" + a + " 01 " + b + " " + c + " " + d + " " + e + " " + f + "\""
  else:
    args = "-t 2000 \"" + a + " 01 " + b + " " + c + " " + d + " " + e + " " + f + " " + g + "\""

  if args:
    executeCommand(True, 2, cmd + " " + args)
  else:
    logger.error('No arguments provided, unable to process command to set screen input')




def replaceLineInFile(fileName, oldLine, newLine):
  """
  Search a file for a line and replace it with a new line

  Parameters:
    fileName: the file to Search
    oldLine: the line to find in the file
    newLine: the line to put into the file after the old line is located and removed
  """
  try:
    for line in fileinput.input(fileName, inplace=1):
      print line.replace(oldLine, newLine).strip()
  except Exception as e:
    logger.error('Error: %s', e)




import ConfigParser
def getConfig(section, variable):
  config = ConfigParser.ConfigParser()
  try:
    config.read("/etc/opt/failover.conf")
    var = config.get(section, variable)
    return var
  except Exception as e:
    logger.error('Error: %s', e)


def initiateFailback():
  replaceLineInFile("/etc/resolution.conf", "HEADS=2", "HEADS=1")

  setScreen("xb", "90")
  time.sleep(5)
  executeCommand(False, 2, "service xwin restart")
  time.sleep(20)

  master = determineMaster()
  reloadMaster(master)


def checkDualHeadStatus():
  try:
    res = open("/etc/resolution.conf", "r")
  except Exception as e:
    logger.error('Unable to open resolution.conf file')
    
  for line in res:
    if "HEADS=2" in line:
      res.close()
      return True

  res.close()
  return False
  
  

def determineNeighbour():
  """
  Determine your neighbour and set some global variables to be used
  """

  hostname = socket.gethostname()
  server = getConfig("server_info", "server_name")
  port = getConfig("server_info", "server_port")
  config_server = server + ":" + port
  save_path = "/var/state/"
    
  logger.debug('Hostname: %s', hostname)

  logger.debug('Fetching data for device %s', hostname)

  deviceURL = "http://" + config_server + "/documents/engine/" + hostname
  downloadFile(save_path, hostname, deviceURL, True)
  engineDoc = open(save_path + hostname)
  deviceData = json.load(engineDoc)
  mz = str(deviceData["marketing_zone_id"])
  engineDoc.close()

  logger.debug('Getting marketing zone data for marketing zone %s', mz) 

  mzURL = "http://" + config_server + "/documents/mz/" + mz
  downloadFile(save_path, mz, mzURL, True)
  mzDoc = open(save_path + mz)
  mzData = json.load(mzDoc)
  try:
    mzFail = str(mzData["takeover_mz_id"])
    mzDoc.close()
  except Exception as e:
    logger.error('No take over MZ found, exiting')
    headStatus = checkDualHeadStatus()
    if headStatus == True:
      initiateFailback()
    sys.exit(27)

  logger.debug('Fetching data for marketing zone ' + mzFail)

  mz2URL = "http://" + config_server + "/documents/mz/" + mzFail 
  downloadFile(save_path, mzFail, mz2URL, True)
  mz2Doc = open(save_path + mzFail)
  mz2Data = json.load(mz2Doc)
  fIP = str(mz2Data["ip_address"])
  mz2Doc.close()
  logger.debug('My neighbour is: %s', fIP)
  return fIP



def monitorConfig(fIP):
  """
  Monitor the configuration on the neighbour media engine

  Parameters:
    fIP: The IP address of the neighbour media engine
  """

  fURL = "http://" + fIP + "/config.json"
  md = "0"
  logger.debug('Using failure URL of %s', fURL)
  try:
    r = requests.head('http://' + fIP + '/config.json', timeout=0.5)
    md = r.headers.get('last-modified')
  except Exception as e:
    logger.error('Error: %s', e)

  logger.debug('Last Modified date returned from the failover host is: %s', md)

  mdFile = "/tmp/modified_date"
  content = readFile(mdFile)

  if not content:
    logger.debug('No content in the file \"%s\", creating the file and writing the last_modified data of \"%s\" to the file', mdFile, md)
    exists = readFile(mdFile, md)
    if exists is True:
      logger.debug('Wrote \"%s\" to \"%s\".', md, mdFile)
    else:
      logger.error('Failed to write \"%s\" to file \"%s\". Are permissions incorrectly set?', md, mdFile)
  else:
    logger.debug('Contents of \"%s\" is \"%s\".', mdFile, content)
    logger.debug('Starting comparison of \"%s\" from file and \"%s\" from the failover host.', content, md)
    status = processChanges( md, content );
    if status is True:
      return True
    else:
      logger.debug('Updating %s with the last modified date of: %s', mdFile, content)
      readFile(mdFile, md)
      return False
 



def reloadMaster(url):
  """
  Call the reload script on the master media engine

  Parameters:
    url: The url to call 
  """
  url = url + "/reload.php"
  try:
    logger.debug('Sending reload request to %s', url)
    req = urllib2.Request(url)
    response = urllib2.urlopen(req)
  except Exception as e:
    try:
      reloadMaster("http://localhost/reload.php")
    except Exception as e:
      logger.error("Unable to reach master for reload, error: %s", e)
    logger.error("Used locahost as master")
  



def monitorSystem(failedStatus, recoverCounter, uptime):
  """
  Monitor the system to determine if failover needs to happen

  Parameters:
    failedStatus: True or False on wheterh or not the system is in a failover state
    recoverCounter: The counter to determine how many cycles the neighbour has been back online
    uptime: the uptime of the neighbour, used for the comparison to determine if it has failed
  """

  timer = 5
  recoveryThreshold = 24
  

  ### Configuration Monitoring
  fIP = determineNeighbour()
  configStatus = monitorConfig(fIP)

  checkFiles = ['config_VGA-0.json', 'media.json', 'playlist.json', 'region.json', 'resources.json', 'schedule.json', 'time_to_play.json']
  for f in checkFiles:
    fileToCheck = '/opt/content2/' + f
    if not os.path.exists(fileToCheck):
      downloadConfigFiles(fIP)

  if configStatus is False:
    downloadConfigFiles(fIP)
    downloadMedia(fIP)

  ### Status Monitoring
  neighbourStatus = compareUptime(fIP, uptime)
  if neighbourStatus is False or neighbourStatus is 0:
    if failedStatus is False:
      logger.critical('Initiating Failover!')
      logger.critical('Neighbour at address %s is offline!', fIP)

      failedStatus = True
      executeCommand(True, 2, "cp -a /opt/content/index-0.html /opt/content/index-1.html")
      executeCommand(True, 2, "cp -a /opt/content/index-0.html /opt/content2/index-1.html")

      replaceLineInFile("/etc/resolution.conf", "HEADS=1", "HEADS=2")
      replaceLineInFile("/opt/content2/config_VGA-0.json", "yes", "no")
      
      # Turn the screen back on here the wait 10 seconds and set the output
      setScreen("ka", "01")
      time.sleep(10)
      setScreen("xb", "70")
      executeCommand(False, 2, "service xwin restart")

      time.sleep(15)
      master = determineMaster()

      reloadMaster(master)

    elif failedStatus is True:
      logger.critical('Device still operating in takeover mode!')
      uptime = 0

  else:
    if failedStatus is True:
      recoverCounter += 1
      logger.critical('Recovery counter currently at: %s of %s', recoverCounter, recoveryThreshold)
      uptime = neighbourStatus
      if recoverCounter == recoveryThreshold:

        initiateFailback()

        failedStatus = False
        recoverCounter = 0
    else:
      uptime = neighbourStatus

  threading.Timer(5, monitorSystem, [failedStatus, recoverCounter, uptime]).start()



def runMonitoring():

  server = getConfig("server_info", "server_name")

  if server == "notconfigured.hostname.org":
    logger.error('Failover configuration server not configured, exiting')
    headStatus = checkDualHeadStatus()
    if headStatus == True:
      initiateFailback()
    sys.exit(27)

  logger.debug('Running runMonitoring()')
  if not "00" in subprocess.check_output("lgutil -t 2000 'kl 01 ff'", shell=True):
    setScreen("kl", "00") # OSD off
    
  setScreen("jq", "00") # Power save off
  setScreen("mi", "00") # Turn off fail over
  setScreen("fe", "e0") # Turn off timer
  setScreen("ff", "00") # Sleep timer off
  setScreen("mn", "00") # Automatic standby off
  setScreen("fg", "00") # Auto Off off
  setScreen("fj", "00") # Disable DPM (display power management)

  logger.debug('starting monitorSystem!')
  monitorSystem(False, 0, 0)


  #try:
  #  server = getConfig('server_info', 'server_name')
  #  port = int(getConfig('server_info', 'server_port'))
  #  sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  #  sock.connect((server, port))
  #  if os.path.exists('/opt/content/config_DVI-0.json'):
  #    logging.critical('System configured for dual head mode, disabling failover.')
  #  else:
  #    logger.debug('starting monitorSystem!')
  #    monitorSystem(False, 0, 0)
  #except Exception as e:
  #  logging.error('Config server not reachable on address: %s and port: %s', server, port)




runMonitoring()



